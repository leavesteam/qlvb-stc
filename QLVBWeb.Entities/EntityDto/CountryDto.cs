﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QLVBWeb.Entities.EntityDto
{
    public class CountryDto : BaseEntityDto
    {
        public int Id { get; set; }
        public string CountryName { get; set; }

    }
}