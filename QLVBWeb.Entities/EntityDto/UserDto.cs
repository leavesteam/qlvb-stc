﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QLVBWeb.Entities.EntityDto
{
    public class UserDto
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
    }
}