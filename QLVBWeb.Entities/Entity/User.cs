﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QLVBWeb.Entities.Entity
{
    public class User : BaseEntity
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Password { get; set; }

        public string Email { get; set; }
        public Nullable<System.DateTime> Birthday { get; set; }
        public string Phone { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastLoginDate { get; set; }
        public DateTime LastPasswordChangeDate { get; set; }
        public bool IsApproved { get; set; }
        public Nullable<bool> IsLock { get; set; }
        public Nullable<bool> IsDisable { get; set; }
        public Nullable<bool> Sex { get; set; }
        public Guid LevelId { get; set; }
        public Nullable<int> ActorId { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<bool> IsSigner { get; set; }
        public string Symbol { get; set; }
        public Nullable<int> Order { get; set; }

    }
}

 
 
     

