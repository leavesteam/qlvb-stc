﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QLVBWeb.Entities.Entity
{
    public class Country : BaseEntity
    {
        public int Id { get; set; }
        public string CountryName { get; set; }
        public string CountryShortName { get; set; }
        public string LogoPath { get; set; }
        public string ISOCodes { get; set; }
    }
}