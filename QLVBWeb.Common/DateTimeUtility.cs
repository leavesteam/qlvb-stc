﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

using System.Web;
using System.Web.Mvc;

using System.IO;

using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace QLVB.Common
{
    public static class DateTimeUtility
    {
        public static DateTime GetFirstDayOfWeek(this DateTime date)
        {
            while (date.DayOfWeek != DayOfWeek.Monday)
            {
                date = date.AddDays(-1);
            }

            return date;
        }
        public static System.DateTime? ConvertToTime(string time)
        {
            System.DateTime? date = null;
            string[] arr = new string[5];

            if (!String.IsNullOrEmpty(time))
            {
                if (time.IndexOf(':') > -1)
                {
                    arr = time.Split(':');
                }
                else if (time.IndexOf('.') > -1)
                {
                    arr = time.Split('.');
                }

                if (arr.Length > 0)
                {
                    int hour = Convert.ToInt32(arr[0]);
                    if (time.IndexOf(' ') > -1)
                    {
                        string[] arr1 = arr[1].Trim().Split(' ');
                        if (arr1.Length > 0)
                        {
                            int minute = Convert.ToInt32(arr1[0]);
                            if (arr1[1].ToUpper() == "PM" && hour < 12)
                            {
                                hour = hour + 12;
                            }

                            date = new DateTime(2001, 1, 1, hour, minute, 0);
                        }
                    }
                }
            }

            return date;
        }


    }
}