﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QLVBWeb.Entities.Entity;

namespace QLVBWeb.DAL
{
    public class UnitOfWork : IDisposable
    {
        private QLVBWebDbContext context = new QLVBWebDbContext();

        //Declarations for Repository
        //private GenericRepository<Entity> nameOfEntityRepository;
        //Begin
        private GenericRepository<Country> countryRepository;
        private GenericRepository<User> userRepository;
        //End

        //Represiontations for Repository Result
        /*Temp-Begin
        public GenericRepository<[Name]> [Name]Repository
        {
            get
            {
                this.nameOfEntityRepository = new GenericRepository<[Name]>(context);
                return nameOfEntityRepository;
            }
        }
        //Temp-End
        */

        //Begin
        public GenericRepository<Country> CountryRepository
        {
            get
            {
                this.countryRepository = new GenericRepository<Country>(context);
                return countryRepository;
            }
        }
        public GenericRepository<User> UserRepository
        {
            get
            {
                this.userRepository = new GenericRepository<User>(context);
                return userRepository;
            }
        }
        //End

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}