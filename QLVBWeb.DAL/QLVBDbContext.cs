﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QLVBWeb.Entities.Entity;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace QLVBWeb.DAL
{
    public class QLVBWebDbContext: DbContext
    {
        public QLVBWebDbContext()
            : base("QLVBWebConnection")
        {
            //Database.SetInitializer(new MyInitializer()); 
            Database.SetInitializer<QLVBWebDbContext>(null); //Not allow drop or Recreate DB
        }

        public QLVBWebDbContext(string connectionString)
            : base(connectionString)
        {
        }

        //Temp
        //public virtual DbSet<AccountType> AccountTypes { get; set; }
        //Temp
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);

            //Binh write
            //throw new Exception("Code first changes are not allowed.");
        }
    }
}