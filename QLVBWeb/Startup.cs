﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(QLVBWeb.Web.Startup))]
namespace QLVBWeb.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
