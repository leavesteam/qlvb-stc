﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QLVBWeb.DAL;
using QLVBWeb.Entities.Entity;
using QLVBWeb.Entities.EntityDto;
using System.Linq.Dynamic;

namespace QLVBWeb.Services
{
    public class UserService
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        public IEnumerable<User> GetAll()
        {
            return unitOfWork.UserRepository.Get(u => u.UserName != "").ToList();
        }
        public string GetById(Guid Id)
        {
            return unitOfWork.UserRepository.Get(m => m.UserName != null).FirstOrDefault(ct => ct.UserId == Id).UserName;
        }
    }
}