﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QLVBWeb.DAL;
using QLVBWeb.Entities.Entity;
using QLVBWeb.Entities.EntityDto;
using System.Linq.Dynamic;

namespace QLVBWeb.Services
{
    public class CountryService
    {
        private UnitOfWork unitOfWork = new UnitOfWork();

        public IEnumerable<QLVBWeb.Entities.Entity.Country> GetAll()
        {
            return unitOfWork.CountryRepository.Get(m => m.CountryName != null).ToList();
        }
        public string GetById(int Id)
        {
            return unitOfWork.CountryRepository.Get(m => m.CountryName != null).FirstOrDefault(ct => ct.Id == Id).CountryName;
        }
    }
}